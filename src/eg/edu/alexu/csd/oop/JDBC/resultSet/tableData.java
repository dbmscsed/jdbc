package eg.edu.alexu.csd.oop.JDBC.resultSet;

import eg.edu.alexu.csd.oop.DBMS.XML.SchemaReader;

import java.util.*;

public class tableData {
    private Map<String ,Integer> names = new LinkedHashMap<>();
    private ArrayList<List<Object>> data = new ArrayList<>();

    public Map<String, Integer> getNames() {
        SchemaReader reader = new SchemaReader();
//        String schemapath = dbName+System.getProperty("file.separator")+parsedData.getTableName()+".xsd";
//        Map<String,String> schemaMap =reader.Read(schemapath);
        return names;
    }

    public void setNames(Map<String, Integer> names) {
        this.names = names;
    }

    public ArrayList<List<Object>> getData() {
        return data;
    }

    public void setData(Object[][] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0;j < data[i].length ; j++)
                this.data.add(Arrays.asList(data[i]));
        }
    }
}
