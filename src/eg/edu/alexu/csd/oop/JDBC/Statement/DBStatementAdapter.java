package eg.edu.alexu.csd.oop.JDBC.Statement;

import eg.edu.alexu.csd.oop.DBMSAdaptar;
import eg.edu.alexu.csd.oop.JDBC.Connection.DBConnectionAdapter;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.*;

public class DBStatementAdapter extends DBStatement{
    Queue<String> SQLQueries = new LinkedList<>();
    int timeOut = 0 ;
    DBMSAdaptar adapter;
    ResultSet resultSet;
    Connection con;
    int updateCount;
    public DBStatementAdapter(DBMSAdaptar adapter,Connection connection) {
        this.adapter= adapter;
        con=connection;

    }


    public void addBatch(String sql) throws SQLException {
        this.SQLQueries.add(sql);
    }

    public void clearBatch() throws SQLException {
        this.SQLQueries.clear();
    }

    public void close() throws SQLException {
        clearBatch();
        adapter=null;
        this.SQLQueries = null;
    }

    public int[] executeBatch() throws BatchUpdateException {

        int[] arr = new int[SQLQueries.size()];

        for(int i = 0 ; i < arr.length ; i++) {

            String sql = SQLQueries.poll();
            Check check = new Check(sql);
            String checker = check.Checker();
            if (checker.equalsIgnoreCase("C")) {

                try {
                execute(sql);
                arr[i] = 0 ;
                } catch (SQLException e) {
                    throw new BatchUpdateException();
                }
            }
            else if (checker.equalsIgnoreCase("U")){

                try {
                    int z = executeUpdate(sql) ;
                    arr[i]  =z ;
                } catch (SQLException e) {
                    throw new BatchUpdateException() ;
                }
            }
            else{
                throw new BatchUpdateException() ;
            }
        }
        return arr ;
    }


    public Connection getConnection() throws SQLException {
        return con ;
    }

    public int getQueryTimeout() throws SQLException {
        return this.timeOut ;
    }

    public void setQueryTimeout(int seconds) throws SQLException {
        this.timeOut = seconds ;
    }
    public boolean execute(String sql) throws SQLException {// excute and if done return true
        ExecutorService excutor =  Executors.newSingleThreadExecutor() ;
        boolean returned  ;
        Future <Boolean> future = excutor.submit(new Task(sql));
        if(getQueryTimeout() != 0 ) {
            try {
                returned =  future.get(getQueryTimeout(), TimeUnit.SECONDS);
                return returned ;

            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (ExecutionException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (TimeoutException e) {
                e.printStackTrace();
                throw new SQLException();

            }
        }else {
            try {
               returned = future.get();
                return returned ;

            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (ExecutionException e) {
                e.printStackTrace();
                throw new SQLException();

            }
        }
    }


    public ResultSet executeQuery(String sql) throws SQLException {
        ExecutorService excutor =  Executors.newSingleThreadExecutor() ;
        Future <Boolean> future = excutor.submit(new Task(sql));
        if(getQueryTimeout() != 0 ) {
            try {
                future.get(getQueryTimeout(), TimeUnit.SECONDS);
                return resultSet ;

            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (ExecutionException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (TimeoutException e) {
                e.printStackTrace();
                throw new SQLException();

            }
        }else {
            try {
                future.get();
                return resultSet  ;

            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (ExecutionException e) {
                e.printStackTrace();
                throw new SQLException();

            }
        }
    }


    public int executeUpdate(String sql) throws SQLException {
        ExecutorService excutor =  Executors.newSingleThreadExecutor() ;
        Future <Boolean> future = excutor.submit(new Task(sql));
        if(getQueryTimeout() != 0 ) {
            try {
                future.get(getQueryTimeout(), TimeUnit.SECONDS);
                return updateCount ;

            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (ExecutionException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (TimeoutException e) {
                e.printStackTrace();
                throw new SQLException();

            }
        }else {
            try {
                future.get();
                return updateCount ;

            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new SQLException();

            } catch (ExecutionException e) {
                e.printStackTrace();
                throw new SQLException();

            }
        }
    }

    class Task implements Callable<Boolean>{

        String query;
        private Object SQLException;

        public Task (String query){
            this.query=query;
        }
        @Override
        public Boolean call() throws Exception {
            Check check = new Check(query);
            String checker =  check.Checker();
            if(checker.equals("C")){
               return adapter.createDatabase(query);
            }
            else if (checker.equalsIgnoreCase("U")) {
                updateCount=adapter.update(query) ;
                return true ;
            }
            else if (checker.equalsIgnoreCase("S")){
                resultSet =executeQuery(query) ;
                return true ;
            }
            else{
               throw new SQLException() ;
            }
        }
    }

}
