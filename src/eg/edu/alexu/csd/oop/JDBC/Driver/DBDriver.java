package eg.edu.alexu.csd.oop.JDBC.Driver;

import eg.edu.alexu.csd.oop.JDBC.Connection.DBConnection;
import eg.edu.alexu.csd.oop.JDBC.Connection.DBConnectionAdapter;

import java.io.File;
import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DBDriver implements Driver {
    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        if (acceptsURL(url)){
            File file = (File) info.get("path");
            String path = file.getAbsolutePath();
            return new DBConnectionAdapter(path);
        }
        throw new SQLException();
    }

    @Override
    public boolean acceptsURL(String url) throws SQLException {
        String URLRegex ="jdbc:+([a-zA-Z])([a-zA-Z0-9_])*:+//+([a-zA-Z])([a-zA-Z0-9_])*";
        Pattern pattern  = Pattern.compile(URLRegex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches())
            return true;
        throw new SQLException();
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
//    Connection conn =
        return new DriverPropertyInfo[0];
    }

    @Override
    public int getMajorVersion() {
        return 0;
    }

    @Override
    public int getMinorVersion() {
        return 0;
    }

    @Override
    public boolean jdbcCompliant() {
        return false;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
