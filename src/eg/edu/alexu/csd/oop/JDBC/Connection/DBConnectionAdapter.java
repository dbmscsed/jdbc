package eg.edu.alexu.csd.oop.JDBC.Connection;

import eg.edu.alexu.csd.oop.DBMSAdaptar;
import eg.edu.alexu.csd.oop.JDBC.Statement.DBStatement;
import eg.edu.alexu.csd.oop.JDBC.Statement.DBStatementAdapter;

import java.sql.SQLException;
import java.sql.Statement;

public  class DBConnectionAdapter extends DBConnection{

    DBMSAdaptar adaptar;
    private String path;
    DBStatement statement;
    DBConnectionAdapter connectionAdapter = null;

    public DBConnectionAdapter getInstance(){
        return connectionAdapter;
    }

    public DBConnectionAdapter(String path){

        adaptar = new DBMSAdaptar();
        try {
            adaptar.createDatabase("Create Database "+path.trim())  ;
            connectionAdapter = this;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Statement createStatement() throws SQLException {

        if(connectionAdapter==null)throw new SQLException();
        if(statement==null){
        statement = new DBStatementAdapter(this.adaptar,this);
        return statement;}
        else{
            return statement;
        }
    }

    public void close() throws SQLException {
            adaptar = null ;
            statement = null ;
            connectionAdapter = null ;
    }
}
