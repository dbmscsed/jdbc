package eg.edu.alexu.csd.oop;

import java.util.LinkedHashMap;
import java.util.Map;

public class tableHeadline extends LinkedHashMap<String,Integer> {
    private static tableHeadline h = null;

    public static tableHeadline getInstance(){
        if (h == null)
            h =  new tableHeadline();
        return h;
    }

    public void setSchemaMap (Map<String,String> schemaMap){
        int i = 0;
        for (Object s : schemaMap.keySet()){
            this.h.put((String) s,i);
            i++;
        }
    }

    public Map<String,Integer> getSchemaMap (){
        return this.h;
    }

    public void removeInstance() {
        this.h = null;
    }
}
