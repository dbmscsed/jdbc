package eg.edu.alexu.csd.oop.JDBC.GUI;

import eg.edu.alexu.csd.oop.JDBC.Driver.DBDriver;
import eg.edu.alexu.csd.oop.JDBC.Driver.DBDriverAdapter;
import eg.edu.alexu.csd.oop.JDBC.resultSet.DBResultSetAdapter;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Callback;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Controller {
    @FXML
    private TextArea batchIn;
    @FXML
    private TextArea batchOut;
    @FXML
    private TextField urlIn;
    @FXML
    private TextField sqlIn;
    @FXML
    private Button connectButton;
    @FXML
    private Button executeButton;
    @FXML
    private Button addToBatchButton;
    @FXML
    private Button executeBatchButton;
    @FXML
    private Button DisconnectButton;
    @FXML
    private Label connectionLabel;
    @FXML
    private TableView tb;
    @FXML
    private GridPane gridPane;
    @FXML
    private Button clearBatchButton;

    private Driver driver = new DBDriverAdapter();
    private Alert alert = new Alert(Alert.AlertType.INFORMATION);
    private Statement st = null;
    private Connection cn = null;
    private boolean isPath = false;
    private String path = null;

    @FXML
    public void connect(){
        try {
            System.out.println(urlIn.getText().indexOf("/")+1);
            if (!isPath)
                this.path = urlIn.getText().substring(urlIn.getText().indexOf("/")+2,urlIn.getText().length());
            isPath = false;
            Properties prop = new Properties();
            File dbDir = new File(path);
            prop.put("path",dbDir);
            cn = driver.connect(urlIn.getText(),prop);
            st = cn.createStatement();
            connectionLabel.setText("Connected");
            connectionLabel.setTextFill(Color.GREEN);
            sqlIn.setEditable(true);
            clearBatchButton.setDisable(false);
            connectButton.setDisable(true);
            DisconnectButton.setDisable(false);
            executeButton.setDisable(false);
            addToBatchButton.setDisable(false);
            executeBatchButton.setDisable(false);

        } catch (SQLException e) {
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Cannot Connect");
            alert.showAndWait();
        }
        catch (Exception e){
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Enter A Valid Input");
            alert.showAndWait();
        }
    }
    @FXML
    public void addToBatch(){
        if (sqlIn.getText().length()!=0){
            batchIn.appendText(sqlIn.getText().trim().concat("\n"));
            try {
                st.addBatch(sqlIn.getText().trim());
                sqlIn.setText("\n");
            } catch (SQLException e) {
                alert.setTitle("Error Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Can't add to Batch");
                alert.showAndWait();
            }
        }
        else {
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Enter A Query to be added");
            alert.showAndWait();
        }
    }
    @FXML
    public void executeBatch(){
        try {
            int [] out = st.executeBatch();
            batchOut.appendText(out.length + " Query executed successfuly + \n");
        } catch (SQLException e) {
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Error Executing Batch");
            alert.showAndWait();
        }
    }
    @FXML
    public void disconnect(){
        try {
            cn.close();
            connectionLabel.setText("Disconnected");
            connectionLabel.setTextFill(Color.RED);
            sqlIn.setEditable(false);
            connectButton.setDisable(false);
            DisconnectButton.setDisable(true);
            executeButton.setDisable(true);
            addToBatchButton.setDisable(true);
            clearBatchButton.setDisable(true);
            executeBatchButton.setDisable(true);
        } catch (SQLException e) {
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Cannot Close Connection");
            alert.showAndWait();
        }
    }
    @FXML
    public void execute(){
        try {
            table(st.executeQuery(sqlIn.getText().trim()));
        } catch (SQLException e) {
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Error Executing Query");
            alert.showAndWait();
        }
    }
    @FXML
    public void fromFile(){
//        try {
            DirectoryChooser dc = new DirectoryChooser();
            dc.setTitle("Choose The DataBase Directory");
            File selectedFile = dc.showDialog(gridPane.getScene().getWindow());
            if (selectedFile != null) {
                Properties prop = new Properties();
                this.isPath = true;
                this.path = selectedFile.getAbsolutePath();
                File dbDir = new File(path);
                prop.put("path", dbDir);
                urlIn.setText("jdbc:DBName://"+path.substring(path.lastIndexOf("\\")+1));
            }
            else {
                alert.setTitle("Error Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Choose A Valid Directory");
                alert.showAndWait();
            }
    }
    @FXML
    public void clearBatch(){
        try {
            st.clearBatch();
            batchIn.setText("");
            batchOut.setText("");
        } catch (SQLException e) {
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Error Deleting Batch");
            alert.showAndWait();
        }
    }

    public void table(ResultSet rs) throws SQLException {
        tb.setEditable(false);
        for(int i=0 ; i<rs.getMetaData().getColumnCount(); i++){
            //We are using non property style for making dynamic table
            final int j = i;
            TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i+1));
            col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList,String>,ObservableValue<String>>(){
                public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                    return new SimpleStringProperty(param.getValue().get(j).toString());
                }
            });
            tb.getColumns().addAll(col);
            System.out.println("Column ["+i+"] "+col.getColumns());
        }
        ObservableList<ObservableList> data = FXCollections.observableArrayList();
        while(rs.next()){
            //Iterate Row
            ObservableList<String> row = FXCollections.observableArrayList();
            for(int i=1 ; i<=rs.getMetaData().getColumnCount(); i++){
                try {
                    row.add(rs.getString(i));
                }
                catch (Exception e){
                    row.add(String.valueOf(rs.getInt(i)));
                }
            }
            System.out.println("Row [1] added "+row );
            data.add(row);
        }

        //FINALLY ADDED TO TableView
        tb.setItems(data);
    }
}
